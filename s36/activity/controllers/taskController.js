// Contains instuctions on HOW your API  will perform its intended task
// All the operations it can do will be placed in this file


const Task = require("../models/task");
// Controller for getting all the tasks 
module.exports.getAllTasks = () => {
	return Task.find({}).then(result =>{
		return result;
	})
};


// controller creating a task
module.exports.createTask =(requestBody) =>{
	let newTask = new Task({
		name:requestBody.name
	})
	return newTask.save().then((task,error)=>{
		if(error){
			console.log(error)
			return false;
		} else {
			return task
		};
	})
}


// controler deleting a task
module.exports.deleteTask = (taskId) => {
  return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
    if (err) {
      console.log(err);
      return false;
    } else {
      return "deleted task.";
    }
  });
};

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result,err)=>{
		if(err){
			console.log(err);
			return false;
		}
		result.name = newContent.name;
		return result.save().then((updatedTask,saveErr)=>{
			if (saveErr){
				console.log(saveErr);
				return false;

			}else{
				return "Task updated.";
			}
		})
	})
}
module.exports.getTask = (taskId) => { return Task.findById(taskId)
    .then(task => {
      if (!task) {
        return { error: "Task not found" };
      }
      return task;
    })
    .catch(error => {
      console.error(error);
      throw new Error("Failed to retrieve task");
    });
};
module.exports.completeTask = (taskId, updatetask) => {
  // Update the task status to complete
  return Task.findByIdAndUpdate(taskId)
    .then((result) => {
      if (updatetask && updatetask.status) {
        result.status = updatetask.status;
        return result.save().then((updatedTask, saveErr) => {
          if (saveErr) {
            console.log(saveErr);
            return false;
          }
          return updatedTask;
        });
      } else {
        console.log("Invalid update task object.");
        return false;
      }
    });
};