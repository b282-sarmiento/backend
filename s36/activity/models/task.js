const mongoose = require("mongoose");
const taskSchema = new mongoose.Schema({
	name : String,
	status :{ 
		type : String,
		default:"pending"

	}
})

//  "modules.exports" is a ways for a node js to treat this value as a "package" that can be used by other files 

module.exports = mongoose.model("task",taskSchema);