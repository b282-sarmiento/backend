 // Defines when particular controllers will be use 
// containe all the endpoints for our applications 
const express = require("express");

// Create a router instance that functions a as a middleware and routing system
const router  = express.Router()
// the "taslController" allows us to use thje funcions defines in ther "taskController.js" file
const taskController = require("../controllers/taskController");

// [Section] Routes
router.get("/",(req,res)=>{
    // invokes the "getAlltask" function from the "controler.js "file and send the result back to the client/program
    taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create a new task
router.post("/",(req, res) =>{
  taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Delete Task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/
router.delete("/:id",(req, res) => {
    taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})
// Updating a task
router.put("/:id",(req,res)=>{
    taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController))
});
// activity
router.get("/:id", (req, res) => {
  // Call the appropriate controller function to retrieve the task
  taskController.getTask(req.params.id)
    .then(resultFromController => res.send(resultFromController))
    .catch(error => {
      console.error(error);
      res.status(500).json({ error: 'Server error' });
    });
});

router.put("/:id/complete", (req, res) => {
  // Call the appropriate controller function to update the task status
  taskController.completeTask(req.params.id,req.body)
    .then(resultFromController => res.send(resultFromController));
});
module.exports = router;
