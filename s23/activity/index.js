// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals
let trainer = {
  name: "Ash",
  age: 26,
  pokemon: ["Pikachu", "Charizard", "Pacquio", "jose"],
  friends: {
    phil: ["john", "david"],
    us: ["ramond", "pedro"]
  },
   talk: function (){
       console.log("Pikachu I choose you") 
    }
};
console.log(trainer)
console.log("result of dot notation")
console.log(trainer.name)
console.log("result of square bracket notation")
console.log(trainer.pokemon)
trainer.talk()

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object
function Pokemon(name, level) {

  this.name = name;
  this.level = level;
  this.health = 2 * level;
  this.attack = level;


  this.tackle = function(target) {
    console.log(this.name + ' tackled ' + target.name);
    target.health -= this.attack;
    console.log("Target Pokemon's health is now reduced to " + target.health);

    if (target.health <= 0) {
      target.faint();
    }
  };

  this.faint = function() {
    console.log(this.name + ' fainted.');
  };
}

let Pacquio = new Pokemon('Pacquio', 10);
let Pikachu = new Pokemon('Pikachu', 3);
let Squirtle = new Pokemon('Squirtle', 1);

console.log(Pacquio);
console.log(Pikachu);
console.log(Squirtle);

Squirtle.tackle(Pacquio);
Pikachu.tackle(Pikachu);
Pacquio.tackle(Squirtle);

console.log(Squirtle);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}