// [SECTION]

// OOP
/*
SYNTAX:
    let objectName = {
        keyA: valueA,
        keyB: valueB,
        ....
    }
*/
let cellphone = {
    name: 'nokia 3212',
    manufactureDate:1999

};


console.log("result from creating objects using initializers/literal noation");
console.log(cellphone);
console.log(typeof cellphone)


// Creating objects using a constructor function
/*
SYNTAX:
 function ObjectName(keyA,KeyB) {
    this.keyA = keyA;
    this.keyB = keyB;

 }
*/

 function Laptop(name,manufactureDate) {
    // "this" keyword allows to assign a nerw object's properties by associating them with values received from constructor function's parameter
    this.name = name;
    this.manufactureDate = manufactureDate;
}

let laptop =new Laptop("lenovo",2008);
console.log(laptop);

// "new" operator creates an intance of an object
let myLaptop = new Laptop("mclenovo",2020);
console.log(laptop)



// [SECTION ]accessing object properties
function calling(){
console.log(myLaptop.name);
console.log(myLaptop['manufactureDate'])
}
calling()

let array = [laptop,myLaptop];
// Accessing array oabjects


console.log(array[0]['name'])

console.log(array[0].manufactureDate)
// [SECTION] Initializing/adding,deleting,Reassining Objects properties
let car = {

};

// Initializing/adding object properties using dot  notation
car.name= "Jark";
console.log(car)
// Initializing/adding object properties using square bracket  notation
car['manufactureDate'] = 2019;
console.log(car)

delete  car["manufactureDate"];
console.log(car);
// delete car.manufactureDate
// console.log(car);

// Reaasigning objerct properties
car.name= "Jark4000";
console.log(car)
// [SECTION] Object Methods 
// A method is a function which is a property of an objects 

let person ={
    name: "john",
    talk: function (){
        //console.log("hello, my name is "+ person.name)
        console.log("hello, my name is "+ this.name)
    }
}
console.log(person);
person.talk()
// [SECTION]  Real World Applications Of Objects 
// Using Objacet Literals

let MyPokemon = {
    name:"pikachu",
    level :4,
    health :100,
    attack : 50, 
    tackle:function(){
    console.log("Pikachu used Tackle!");},
    faint: function(){
    console.log("Pikachu fainted...")
  }
}
console.log(MyPokemon)
MyPokemon.tackle();
MyPokemon.faint();

function Pokemon (name, level){
    //  properties 
        this.name = name;
        this.level = level;
        this.health = 2 * level;
        this.attack = level;
    // method 
        this.tackle = function(target){
            console.log(this.name + 'tackled'+ target.name);
            console.log("targetPokemon is health is now reduced to " + Number(target.health - this.attack));
        }
        this.faint = function() {
        console.log(this.name+ ' fainted.')
    }
}
let pacquio = new Pokemon('Pacquio',100);
console.log(pacquio)

let pikachu = new Pokemon("Pikachu",5)
console.log(pikachu)

pacquio.tackle(pikachu)

pikachu.faint()