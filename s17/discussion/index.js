// console.log("hello world")
declaredFunction();
// [SECTION] Functions
// funtions in js are lines/block of codes that tell our devices/application to perform a certain task when called/invoked

// Function declarations 
/*
SYNTAX: 
    function funtionName(){
        code block(statement)
    }
*/
// function - keyword used to define a JS functions 
// printName are named to be able to be used later in the code
function printName() {
    // function block  {}-the statements which comprise the body of the function
    console.log("My name is JL");
}
// function invocation
printName();
declaredFunction();
// [SECTION] funtion declarations vs expression  
// funtion declarations 
// A funtion can be created through function declaration by using a funtion keyword and adding a function name
function declaredFunction() { 
    console.log("Hellow Wolrd from declaredFuntions()!")

}
declaredFunction();

// Function Expression
// A funtion can also be stored in a variable 
// a funtion cexpression is an anonymous funtion assigned to a variableFunction
// Anonymous funtion- funtion without a name
let variableFunction = function(){
    console.log("hello again ");
} 
variableFunction();
// Funtion expressions are always invoked/called using variable name
let funcExpression = function funcName() {
    console.log("hello from the other side!")
}
funcExpression()
// funcName() funcName is not defined 

// you can  reassign funtions declarations and function expression to new anonymous functions.

 declaredFunction = function(){
    console.log("updated declared moiney-moiney")
 }
declaredFunction()

funcExpression = function () {
    console.log("Updated funcExpression")
}
funcExpression()

const constantFunc = function(){
    console.log("Initialized with const!")
}
constantFunc()

// constant funtion can't be reasign 

// [SECTION]Funtion scoping
/*
JS has 3 types of scopes
1.local/block scope
2.global scope
3.funtion scope
*/
{
    let localvar = "JL";
    console.log(localvar)
}
let globalVar = "Mr.Worldwide";

console.log(globalVar)
// console.log(localvar)


function showNames(){
    // funtion scoped variables
    var funtionsVar = "Joe";
    const funtionConst = "john"
    let functionLet = "jane"
    console.log(funtionsVar)
    console.log(funtionConst)
    console.log(functionLet)
}
showNames()

// console.log(funtionsVar)  will result to error 
    // console.log(funtionConst) will result to error 
    // console.log(functionLet) will result to error 

let globalName = "Alexandro"
function myNewFuntion2(){
    console.log(globalName)
    variable3= "hello world"
}


// Nested funtions 
// You can create another funtions inside a funtion

function myNewFunction(){
    let name ="JL";

    function nestedFunction(){
        let nestedName = "saki"
        console.log(name);
    }
  
    nestedFunction()
}
myNewFunction();

// [SECTION] Using alert()
// alert()allows us to show a small window at the top of our browser page to shgow information to our users


// alert("Hellow World!"); //this will run immediately When the page loads


// function showSampleAlert(){
    // alert("Hello, User!")
// }

// showSampleAlert();
 // console.log("I will only log in the console when the alert is dismissed5")
 // [SECTION] Using prompt()
// prompt() allows us to show asmall window at the top of the browser to gather user imput.

let samplePrompt = prompt("Enter your name");

console.log("hello",samplePrompt);

function printWelcomeMessage(){
    let firstName = prompt("enter your first name.")
    let lastName = prompt("enter your last name.")
    console.log("Hello ",firstName,lastName)
    console.log("Welcome to my page!")
}
printWelcomeMessage();

// [SECTION] Funtion naming Conventions 
// Function names should be definitive of the task  it will perform . 
// It usually contains a verb.
function getCourses(){
    let courses = ["science 101","math 101", 
    "english 101"]
    console.log(courses)
}

getCourses()
// name your functions in small caps 
// Follow camelCase when namingg variables
function displayCarInfo(){
console.log("brand: toyota")
console.log("type: Seadan")
console.log("Price: 1,500,000")
}
displayCarInfo();

// Avoid generic names to avoid confusion within you code 
function get(){
    let name = "JL";
    console.log(name)

}
get();
//  Avoid pointless and inappropriate funtion name
function foo(){
    let monkey = "gugaga"
    console.log(monkey)
}
foo()