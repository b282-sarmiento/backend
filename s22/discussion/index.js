// console.log("hello Human")
// Array Methods

// JS has built-in functions and methods for arrays
// this allows us to manipulate and access array items
// [SECTION] Mutator Methods 
// Mutator methods are functions that mutate or change an array after they are created .

let fruits = ["Apple", " orange" , "kiwi" , "Dragon fruit"]
console.log("Original Array")
console.log(fruits);// ["apple", " orange" , "kiwi" , "Dragon fruit"]

// push()
// adds an element in the end of the of an aray and returns the updated array's length
/*
SYNTAX:
    arrayName.push();
*/
let fruitsLength =fruits.push("Mango");
console.log('fruitsLength:')
console.log(fruitsLength)//
console.log("Mutated array from push() method:")
console.log(fruits);

fruitsLength =fruits.push("Avocado","Guava");
console.log('fruitsLength:')
console.log(fruitsLength)//
console.log("Mutated array from push() method:")
console.log(fruits);// ['apple', ' orange', 'kiwi', 'Dragon fruit', 'Mango', 'avocado', 'Guava']

// pop()
// Removes the last element and return the removed element

/*
SYNTAX:
    arrayName.pop();

*/
console.log('Current Array');
console.log(fruits);
let removedFruit = fruits.pop();

console.log("Removed fruit:");
console.log(removedFruit);//
console.log("Mutated array from pop() method:")
console.log(fruits);


// unshift()
// it adds one or more elements at the BEGINNING of an array and it returns the updated array's length


/*
SYNTAX:
SYNTAX:
    arrayName.unshift('elementA');
    arrayName.unshift('elementA','elementB');
*/
console.log('Current Array');
console.log(fruits);
fruitsLength = fruits.unshift('Lime','Banana');
console.log(fruitsLength)// ['Apple','Orange','kiwi','Dragon fruit','Mango','Avocado']
console.log("Mutated array from unshift() method:")
console.log(fruits);//['lime', 'Banana', 'apple', ' orange', 'kiwi', 'Dragon fruit', 'Mango', 'avocado']

// shift()
// removes an element at the beginning of an array and returns the remove element
/*
  arrayName.shift();
*/

console.log("Current Array");
console.log(fruits);////['lime', 'Banana', 'apple', ' orange', 'kiwi', 'Dragon fruit', 'Mango', 'avocado']

removedFruit = fruits.shift();
console.log("removed fruit")
console.log(removedFruit);//Lime
console.log('mutated array from the shift()method:');
console.log(fruits)//['Banana', 'apple', ' orange', 'kiwi', 'Dragon fruit', 'Mango', 'avocado'


// splice()
// Use the splice() method to remove elements from a specific position in the array.
// simultaneosly removes an elements from a specified index number and adds element

/*
SYNTAX
    arrayName.splice(startingIndex,deleteCount,elementsToBeAdded)
*/

console.log("Current Array:");
console.log(fruits)
//['Banana', 'apple', ' orange', 'kiwi', 'Dragon fruit', 'Mango', 'avocado']

fruits.splice(2,1,'lime');

console.log('muated array from splice method:');
console.log(fruits)


// sort()
// rearranges the array lement in alphanumeric order
/*
SYTANX:
    arraynName.sort()

*/
console.log('Current Array')
console.log(fruits);
fruits.sort();
console.log('Mutated array from the sort () method:');
console.log(fruits);


// Reversed ()
// reverses the order of array elements 
/*
SYNTAX:
    arrayName.reverse():"
*/
console.log('Current Array')
console.log(fruits);
fruits.reverse();
console.log('Mutated array from the reverse () method:');
console.log(fruits);


// [SECTION] None-mutator methods
// None mutator methods are functions taht do not modify or change an array after they are created .

let countries = ['US','PH','CA','SG','TH','PH','FR','DE'];
console.log(countries);
// indexof()
// returns the index number of the first matching element found in an array
/*
SYNTAX:
    arrayName.indexOf(searchValue);
    arrayName.indexOf(searchValue,Starting index);
*/
let firstIndex = countries.indexOf('PH');
console.log('Result of firstIndex')
console.log(firstIndex)

firstIndex = countries.indexOf('PH',2);
console.log(" Result of firstIndex ")
console.log(firstIndex)

// if no match was found, result will always be -1
firstIndex = countries.indexOf('Uk');
console.log(" Result of firstIndex ")
console.log(firstIndex)

// lastIndexOf()
// returns the index number of the last matching element in an array
/*
SYNTAX:
    arrayName.lastIndexOf(searchValue);
    arrayName.lastIndexOf(searchValue,Starting index);
*/

// let countries = ['US','PH','CA','SG','TH','PH','FR','DE'];
let lastIndex = countries.lastIndexOf('PH')
// slice
// portions/slices elements from an array and return a new array
/*
SYNTAX:
     arrayName.indexOf(startingIndex);
    arrayName.indexOf(Starting index, endingIndex);

*/

// let countries = ['US','PH','CA','SG','TH','PH','FR','DE'];

let  slicedArrayA= countries.slice(2);
console.log('Result from slice() method:');
console.log(slicedArrayA);(6)//['CA', 'SG', 'TH', 'PH', 'FR', 'DE']
let  slicedArrayB= countries.slice(2,4);
console.log('Result from slice() method:');
console.log(slicedArrayB)
// toString()
// return an array as string separeted by commas

/*
SYNTAX:
    arrayName.toString();    
*/

let stringArray = countries.toString()
console.log('Result from toString method:');
console.log(stringArray);
console.log(typeof stringArray)
// [SECTION] Iteration Methods
// loops designed to perform repetitive task
// loops over all items in an array

// forEach()
// similar to a for loop that iterates on each array of elements

/*
SYNTAX:
    arrayName.forEach(function(indivElement){
    statement
    }
    )
*/

countries.forEach(function(country){console.log(country);

})

//  for (let i = 0; i < countries.length; i++) {
//   console.log(countries[i]);
// }
// console.log(p)

// map()
// iteraate on eac h lement and return new array with different values depending on the result of the function's operation

/*
let/const resultArray = arrayName.map(function(indivElement){
    statement
})
*/

let numbers = [1,2,3,4,5]

let numberMap = numbers.map(function(number){
    return number * number;
})
console.log("Original Array:")
console.log(numbers);
console.log("Result of map() method");
console.log(numberMap)

const numbers1 = [1, 2, 3, 4, 5];

const doubledNumbers = numbers1.map(function(number) {
  return number * 2;
});


// filter()
// return new array that contains new elements which meets the given conditions

/*
let/const resultArray = arrayName.filter(function(indivElement){
    statement
})
*/

let filterValid = numbers.filter(function(number){
    return (number < 3 );
})
console.log("Result of filter method")
console.log(filterValid);
// includes
// check if the argument passed can be found in the array
/*
arrayName.includes(<argumentTofind)
*/

let product = ["Mouse", "Keyboard", "Laptop","Monitor"]
let productFound1 = product.includes("Mouse")
console.log("Result from includes() method:")
console.log(productFound1)