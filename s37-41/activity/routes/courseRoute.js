
const express = require("express");

const router = express.Router();

const auth = require ("../auth")

const courseController = require("../controllers/courseController")




// // route for creating course
// router.post("/create", auth.verifyAdmin, (req, res) => {
//     const userData = auth.decode(req.headers.authorization);

//     courseController.addCourse(req.body, userData).then(resultFromController => res.send(resultFromController));
// });



// module.exports = router
// S39 Activity Start
router.post("/create", auth.verify, (req, res) => {

    const data = {
        course: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }

    courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});
// S39 Activity End
// Route for retrieving all the Courses
router.get("/all",(req,res)=>{
    courseController.getAllCourses().then(resultFromController =>res.send(resultFromController))
});



// route for retrieving all coureses
router.get("/active",(req,res)=>{
    courseController.getAllActive().then(resultFromController =>res.send(resultFromController))
});


// Route for retrieving specific course
router.get("/:courseId",(req, res)=>{
    courseController.getCourse(req.params).then(resultFromController =>res.send(resultFromController))

})



router.put("/:courseId", auth.verify, (req, res)=>{
    courseController.updateCourse(req.params, req.body).then(resultFromController =>res.send(resultFromController))
})

router.patch("/:courseId/archive",auth.verify,(req,res)=>{
    courseController.archiveCourse(req.params, req.Body).then(resultFromController => res.send(resultFromController));
}) 


module.exports = router;