const express = require("express");
const mongoose = require("mongoose");
// allows our backend applications to be available to our frontend applications 
const cors = require("cors");
const userRoute = require("./routes/userRoute")
const courseRoute = require("./routes/courseRoute")

const app = express();



// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://jlsarmiento1996:saki12345@wdc028-course-booking.9nbzgsx.mongodb.net/courseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
// Add the tasks route
app.use(cors());

// Defines the "/courses" string to be included for all the user routes defines in the "coure"
app.use("/users",userRoute);
app.use("/courses",courseRoute)


// cors - allows all resources to access our backend application
app.listen(process.env.port||4000,()=>console.log(`Now listening to port ${process.env.PORT || 4000}!`));
 