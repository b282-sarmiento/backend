let http = require("http");
const port = 3000;
const server = http.createServer((request, response) => {
  if (request.url == '/greetings') {
    response.writeHead(200, {'Content-type': 'text/plain'});
    response.end('Hello world!');
  } else if (request.url == '/homepage') {
    response.writeHead(200, {'Content-type': 'text/plain'});
    response.end('This is the homepage!');
  } else{
  	response.writeHead(200, {'Content-type': 'text/plain'});
    response.end('Page is not available');
  }
});
server.listen(port);

console.log(`Server running localhost:${port}`);