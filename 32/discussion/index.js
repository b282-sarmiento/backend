// create a server  
// "require" directive used to load node.js modules
// "http" module that allows node.js to transfer data using hyper text transfer protocol (HTTP)
let http = require("http");
// "createServer()" method used to create an HTTP server that listens to request on a specified port and give responses back to the client

// "request" - messagees sent by the  client (usually a web browser)

// response messages sent by ther server as an answer 
http.createServer(function(request,response){
  // writeHead()method used to set a status code for response and set the content type of the response
  response.writeHead(200,{'Content-type':'text/plain'})
  // end() method used to send the response with text content "hello world!"
  response.end('hellow world!')
// .listen(portNumber) method is node.js used to start a sever listening for incoming connections on a specified port
  // port = 4000
  }).listen(4000)
  console.log('server running local host: 5000');

/* [kill port]
npx kill-port portNumber
npx kill-port 4000
*/