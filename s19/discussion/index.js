// [SECTION] if, else if , and else statements 
// if Statements 

let numA = -1
if (numA < 0) {
    console.log("Hello!");
} 



let numB = 2

if (numB < 0){
    console.log("Hello!")
}
// else if clause
// execute a statement if previous conditions are false and if the specified conditions is ture
else if  (numB > 0){
    console.log("Not Hello")

}

let numC = 2

if (numC < 0){
    console.log("Hello!")
}
// else if clause
// execute a statement if previous conditions are false and if the specified conditions is ture
else if  (numC == 0){
    console.log("Not Hello")

}
// else statements
else {
    console.log("hello but not hello")
}


// windSpeed < 30 → Not a typhoon yet.
// windSpeed <= 61 → Tropical depression detected.
// windSpeed >= 62 && windSpeed <= 88 → Tropical storm detected.
// windSpeed >= 89 && windSpeed <= 117 → Severe tropical storm detected
// if, else if, and else statements with functions 
let message;
function deterTypInt(windspeed) {
   if (windspeed < 30) {
     return "not a typhoon " 
    }

    else if  (windspeed <= 61) {
        return "Tropical storm detected."
    }
      else if  (62 && windspeed <= 88 ) {
        return "Severe tropical storm detected "
    }
      else if  (89 && windspeed <= 117) {
        return "Severe tropical storm detected "
    }

    else {
        return "Typhoon"
    }



}


message = deterTypInt(70)
console.warn(message)

if (message == "Tropical Storm Detected") {
    // console.warn () a good way to print warnings in our console that could help us developers act on a certain output within our code
    console.warn(message)
}

// [SECTION] Truthy and Falsy 
//  in JS a "truthy" value  is a value that is considered true when encountered in a Boolean context .

/*
1.false
2.0
3.-0
4.null
6.undefined
7.Nan
*/

if(true) {
    console.log('Truthy');
}


if(1) {
    console.log('Truthy');
}


if([]) {
    console.log('Truthy');
}

// 

if(undefined){
    console.log('falsy')
}
// [SECTION] Conditional (ternary)Operator
// Ternary operatos have an implicit "retuirn " statement


/*
The conditional (ternary)operator takes in 3 operands
1. condition
2. expression to execute if the condition is truthly
3. expression to execute if the condition is falsy
*/

/*
SYNTAX
    (Condition) ? it true : if false;

*/
let ternaryResult = (1 < 18) ? true : false;
console.log("result of ternary Operator: "+ternaryResult)


let ternaRyResult = (1 > 18) ? true : false;
console.log("result of ternary Operator: "+ternaRyResult)


let name;

function isOfLegalAge(){
    name = 'john'
    return 'you are of the legal age limit'
}




function isUnderAge(){
    name = 'jane'
    return 'you are under legal age limit'
}
// The "parseInt"function converts the input received into number datatype
// let age  = parseInt(prompt ("what is your age?"));
// console.log(age);

// let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator in functions: " + legalAge + ', ' + name);


// [SECTION] Switch Statement
// can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an expected input
// SYNTAX:
//     switch (expression) {
//         case value:
//         statement;
//         break
//     default:
//         statement;
//         break;
//     }
//     
// "toLowerCase" = funtion or method wil lchange the input receive from the prompt into all lowercase letters 
// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch (day) {
//   case 'monday':
//     console.log("The color of the day is red");
//     break;
//   case 'tuesday':
//     console.log("The color of the day is orange");
//     break;
//   case 'wednesday':
//     console.log("The color of the day is yellow");
//     break;
//   case 'thursday':
//     console.log("The color of the day is green");
//     break;
//   case 'friday':
//     console.log("The color of the day is blue");
//     break;
//   case 'saturday':
//     console.log("The color of the day is indigo");
//     break;
//   case 'sunday':
//     console.log("The color of the day is violet");
//     break;
//   default:
//     console.log("Invalid day");
// }
// [Section] Try-catch-finaly statement
// "try catch" statements are comonly used for error handlign

function ShowIntensityAlert(windspeed){
    try{
        // attempt to execute a code
        alerat(deterTypInt(windspeed));   
    } catch (error){
        // typeof operator is used to check the data type of a vale/expression and return a string value of what the data type is 
        console.log(typeof error);
        // "error.message" - is used to access the information relating to an error object
        console.warn("blasdaw");
    }
        finally {
            alert('intensity updates will show new alert')
        }
}
ShowIntensityAlert(20)