// console.log("JJavaScript ES6 Updates")
// [SECTION ]Exponent Operator 

// Math.pow 
// - method takes two arguments , the base and exponent 
// raise to the power of two
const firstNum = Math.pow(8,2);//64
console.log(firstNum);
// ** operator is used for exponentiation 
const secondNum = 8 ** 2;
console.log(secondNum)
// [SECTION] template literals 
// allows to write strings without using concatenation operator "+"
let name = "John";
let message = 'Hello '+ name + '! Welcome to programing!'
console.log(message);
message = `Hello ${name}! Welcome to programing!`;
console.log(message);
const interestRate = .1
const principal = 1000;
const inter = principal * interestRate
console.log(`The interest on your sav ings account is: ${inter} and ${principal * interestRate}`)

// The interest on your savings account is :100

// [SECTION] Array Destructuring
// Allows to unpack elements in arrays into disctinc variables

/*
let/const [variableName, variableName,
variableName..] = arraynaMe
*/

const fullname = ['juan', 'Dela', 'Cruz']
console.log(fullname[1])

const[firstName, middleName, lastName] = fullname;
console.log(middleName);

// [SECTION] Object Destructuring
// Allows to unpack elements in object into disctinc variables

/*
let/const {variableName, variableName,
variableName..} = objectName

*/

const person = {
givenName:"Jane",
maidenName:"Dela",
familyName:"Cruz"

};

console.log(person.familyName)

const{
    givenName,maidenName,familyName 
} = person;
console.log(familyName)

// [SECTION] arrow function
// Arrow functions allow us to write shroter function syntax
/*
SYNTAX : 
    let/const variableName = () =>{
    statement
    }
*/
const student = ['john','jane','judy']
console.log("result from using normal function")
student.forEach(function(student){
    console.log(`${student} is a student.`);})

// arrow function 
console.log("result from using arrow function")
student.forEach((student)=> {console.log(`${student} is a student.`);})

// [SECTION]Implicit Return Satatement 
// there are intanc es when you can omit ther "return" statement

// const add = function(x,y){
//     return x + y
// }

// const total = add(1,1)
// console.log(total)

console.log("result from using arrow function")

const add = (x,y)=> x+y;

const total = add(1,1)
console.log(total)
// [SECTION] default function Argument value
const greet = (name = "user") =>{
    return `Good morning,${name}`
}
// [SECTION] Class-Based object Blueprints
// Allows creation/instatiation of object using classses as blueprints 
class Car {
    constructor(brand,name,year){
        this.brand = brand;
        this.name = name;
        this.year = year;
    }
}
const myCar = new Car()

console.log(myCar);
// creating/instantiating a new object from a car class with initilized values
const myNewCar = new Car("Lamborgini", "GT 2000", 2021)

console.log(myNewCar)
console.log(myNewCar.name)
myCar.brand = "toyota"
myCar.name = "rio"
myCar.year = 1996


console.log(myCar)