// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


// Using the aggregate methond 
/*
SYNTAX:
  db.collectionName.aggregate([
    {$match: {field: valueA}},
    {$group: {_id: "$fieldB"}, {result: {operation}}}
])


*/
// $ symbol will refer to a field name that is available in the documents that are being aggregated on . 



db.fruits.aggregate([
  // match is used to pass documents that meet specified condition(s) to the next pipeline stage/ aggregration process
  {$match:{onSale:true}},
  // $group is used to group elements together and field-value pairs using the data from the grouped elements0
  {$group:{_id:"$supplier_id",total:{$sum:"$stock"}}},
  {$project :{_id:0}}
  ]);
// Field projection with aggregation 
/*
$projcect : {field:1/0}

*/
db.fruits.aggregate([
  {$match:{onSale:true}},
  {$group:{_id:"$supplier_id",total:{$sum:"$stock"}}},
  {$project :{_id:0}}
  ]);


// SYNTAX:
  /*
{sort: {field:1/-1}}
{ $sort: { <field1>: <sort order>, <field2>: <sort order> ... } }
  */

  // Sorting aggregated result 
  db.fruits.aggregate([
  {$match:{onSale:true}},
  {$group:{_id:"$supplier_id",total:{$sum:"$stock"}}},
  {$sort:{_id:-1}}
  ]);
  // --
  db.fruits.aggregate([
  {$match:{onSale:true}},
  {$group:{_id:"$supplier_id",total:{$sum:"$stock"}}},
  {$sort:{total:-1}},
  ]);
// $unwind deconstructs an array field from a collection with an array value to output a result for each element
/*SYNTAX:
{unwind:field}
*/
db.fruits.aggregate([
  {$unwind:"origin"}
])
