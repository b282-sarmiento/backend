// Crud Operations

// Create Operations
// insertion() - Inserts one document to the collections 
db.users.insertOne({
    "firstName": "John",
    "lastName":"smith"
});
// insertMany()insert multiple documents to the collection
db.users.insertMany([
{"firstName":"john","lastName":"Doe"},
{"firstName":"jane","lastName":"Doe"}
]);

// read operation
// find()- get all the inserted users 
db.users.find();

// Retrieving specific Documents
db.users.find ({"lastName":"Doe"});

// Update Operations 
// updateOne()-modify one document
db.users.updateOne(
{
"_id" : ObjectId("648af92ffe1e352b75a8230c")
},
{
    $set:{
        "email":"johnsmith@gmail.com"
    }
}
);

// updateMany() - modify multiple documents  
db.user.updateMany(
  { "lastName": "Doe" },
  { $set: { "isAdmin": false } }
);
// Delete Operation
// deleteMany-deletes multiple documents
db.users.deleteMany({"lastName:Doe"})
db.users.deleteMany({ "lastName": "Doe" });
db.users.deleteOne({ "_id" : ObjectId("648af92ffe1e352b75a8230c")});



//This will retrieve all the documents?
//db.users.find()

//Retrieve a document that has Stephen as its firstName
db.users.find({ "firstName": "Stephen" });

//Retrieve document/s with 



db.users.find({ "firstName": "Stephen" });

//Retrieve document/s with Armstrong as its lastName and 82 as its age
db.users.find({ "lastName" : "Armstrong", "age" : 82});

