// console.log
// ("hello world")
// Functions 
// function printInput(){
//     let nickname = prompt("Enter your nickname:");
//     console.log("Hi "+ nickname);

// }
// printInput()

// Parameter and Arguments 
// (name) - is called parameter
// A "parameter" acts as a named variable/ conainer that exist ionly inside a function
function printName(name){
    console.log("My name is " + name)
}
// (juana)-is caled argument (the information/data provided diretly into the fuction is called an argument)
printName("Juana");
printName("John");
printName("Jane");
// Variables can also be past an argument
let sampleVariable = "Yui";
printName(sampleVariable)

function checkDivisibilty(number){
   let remainder = number % 8;
   console.log("The remainder of "+number+ " divided by 8 is: " + remainder)
   let idDivisible = remainder === 0;
   console.log("is " + number + ' divisible by 8?');
   console.log(idDivisible);
}
checkDivisibilty(64)
checkDivisibilty(5)


// Functions as Arguments
function argumentFunction(){
    console.log("this function was passed as an argument before the message was printed")
}

argumentFunction()

function invokeFunction(x){
    argumentFunction()
}

invokeFunction(argumentFunction)

// Multiple "Arguments" will corrrespond to the number of " parameters " declared in a function in suceeding order.log

function createFullName(firstName, middleName, lastName){
    console.log(firstName+' '+middleName+' '+lastName) 
}

createFullName('juan','Dela',"Cruz");
createFullName('juan','Dela');
createFullName('juan','Dela',"Cruz","Hello");

// Using variables as arguments 
let firstName ="john";
let middleName = "Doe";
let lastName = "Smith"
createFullName(firstName,middleName,lastName)