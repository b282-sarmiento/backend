//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('<urlSample>')
       .then((response) => response.json())
       .then((json) => json)
   
   
   );

}





// Getting all to-do list items
async function getAllToDo() {
  return await fetch("https://jsonplaceholder.typicode.com/todos")
    .then((response) => response.json())
    .then((json) => json.map((key) => key.title));
}
getAllToDo().then((result) => console.log(result));
// [Section] Getting a specific to do list item
async function getSpecificToDo(){

  
   return await (fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then((response) => response.json())
    .then((json)=>json))
    
   


}
getSpecificToDo().then((result) => console.log(result));

// [Section] Creating a to do list item using POST method

async function createToDo(){
   
   return await (
    fetch('https://jsonplaceholder.typicode.com/todos',{method: 'POST',
headers: {
    'Content-type' : 'application/json'
},
body: JSON.stringify({

    userId: 1,
    id: 201,
    title: "Created to do list items",
    completed: false
  
})
})
.then((response) => response.json())
.then((json)=>json))

}

createToDo().then((result) => console.log(result,),console.log('The item "delectus aut autem" on the list has the status of false'));



// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (fetch('https://jsonplaceholder.typicode.com/todos/1',{method: 'PUT',
headers: {
    'Content-type' : 'application/json'
},
body: JSON.stringify({
    DateCompleted:"Pending",
    descripttion:"to update the my to do list with a different data structure",
    userId: 1,
    id:1,
    status : "pending",
    title: "Updated to do list",
    completed: false
  
})
})
.then((response) => response.json())
.then((json)=>json))


}
updateToDo().then((result) => console.log(result));
async function updateToDo1(){
   
   return await (fetch('https://jsonplaceholder.typicode.com/todos/1',{method: 'PUT',
headers: {
    'Content-type' : 'application/json'
},
body: JSON.stringify({
    completed:false,
    DateCompleted:"07/09/21",
    userId: 1,
    id:1,
    status : "Complete",
    title: "delectus aut autem",
   
})
})

.then((response) => response.json())
.then((json)=>json))
  
}

updateToDo1().then((result) => console.log(result));
// // [Section] Deleting a to do list item
async function deleteToDo() {
  const response = await fetch('https://jsonplaceholder.typicode.com/todos/1', {
    method: 'DELETE'
  });

  const json = await response.json();
  return json;
}




deleteToDo().then((result) => console.log(result))

//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}













