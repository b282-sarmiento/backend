// [SECTION] Javascript Synchronous and Asynchronous
// JS is by default is synchrounous, meaning that only one statement is execute at a time 
console.log('Hello World')
console.log('Hello world Again!')
// Asynchronous means that we can proceed to execute other statements,consuming is runnig in the background 
// [SECTION] Getting all posts 
// "Promise" is an object that represen ts the eventual complition or failure of an asynchronous function and its resulting value
/*
SYNTANX:
	fetch('url')

*/
console.log(fetch('https://jsonplaceholder.typicode.com/posts/'));
// fetch and .then 
/*
SYANTAX : 
  fetch('Url').then(response=>{})
*/
// "fetch" method will return a "promise" that resolves to a "response object"
fetch("https://jsonplaceholder.typicode.com/posts/").then(response => console.log(response.status));
// ".then" method captures the "response object" and return  another promise which will be eventually be resolved or rejected 
fetch("https://jsonplaceholder.typicode.com/posts/")
// ".json" method from the response object to convert the data retrieved into JSON format to be used in our application
.then((response) => response.json())
.then((json)=>console.table(json));



// // Async  and Await 
// async function fetchData(){
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts/');
// 	console.log(result);
// 	console.log(typeof result);
// 	console.log(result.body);

// 	let json = await result.json()
// 	console.log(json);

// }
// fetchData();

// [SECTION] Creating a post
fetch('https://jsonplaceholder.typicode.com/posts/',{method: 'POST',
headers: {
	'Content-type' : 'application/json'
},
body: JSON.stringify({

    userId: 1,
    id: 101,
    title: "Create",
    body: "Body of Post"
  
})
})
.then((response) => response.json())
.then((json)=> console.log(json));


// [SECTION] Updating a post 
fetch('https://jsonplaceholder.typicode.com/posts/1',{method: 'PUT',
headers: {
	'Content-type' : 'application/json'
},
body: JSON.stringify({

    userId: 1,
    title: "UPDATE",
    body: "UPDATE BODY"
  
})
})
.then((response) => response.json())
.then((json)=> console.log(json));


// [SECTION] delete post
fetch('https://jsonplaceholder.typicode.com/posts/1',{method: 'Delete',
headers: {
	'Content-type' : 'application/json'
}

})
.then((response) => response.json())
.then((json)=> console.log(json));

