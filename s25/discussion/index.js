console.log("hello world")

/*
JSON
-stands for javascript object notation
-is also used in other programming languanges
-Core Javascript has a build in JSON object that contains method for paring JSon objects and coverting strings into JavaScript Objects



[SYNTAX] :
{
	propertyA: "valueA",
	"propertyB": "valueB"
};

JSON are wrapped in curly braces 
Properties and values are wrapped in double
*/

let sample1 = `
	
	{
		"name":"Mochi",
		"age":20,
		"address":{
			"city" : "Tokyo",
			"country":"Japan"
		}

	}
`;
console.log(sample1);


// create a variable that will hold a "jason" and create a [person object]


let person = `
	{
		"FirstName":"JL",
		"LastName":"Sarmiento",
		"age":26,
		"address":{
			"city" : "Tokyo",
			"country":"Japan"
	}
}
`;

console.log("person")
console.log(person)
console.log(typeof person)




// Are we able to turn a JSON into A JS Object?
// JSON.parse()- will return the JSON as an object

console.log(JSON.parse(sample1));
console.log(JSON.parse(person));


// [JASON ARRAY]
// JSON array is an array JSON

let sampleArr = `

[
  {
    "email": "moci@gmail.com",
    "password": "mochimochi",
    "isAdmin": false
  },
  {
    "email": "zenitsu@proton.me",
    "password": "agatsuma",
    "isAdmin": true
  },
  {
    "email": "jsonv@proton.me",
    "password": "friday13",
    "isAdmin": false
  }
]

`;
console.log(sampleArr)
console.log(typeof sampleArr)

// can we use array methods on a JSON array?(sampleArr)
// No. Because a JSon is a string


let parsedSampleArr = JSON.parse(sampleArr);

console.log(parsedSampleArr);//
console.log(typeof parsedSampleArr);//
// can we now delete the last item in the JSON array?
// yes 
// what method should we use?
// pop()
console.log(parsedSampleArr.pop())
console.log(parsedSampleArr)


//if for example, we need to send this data bac k to our client/front End, it should be in JSON Format 
// JSON.stringify - this will convert the JS onjects to strings - this will stringify Js object as Json
// JSON.parse()- does not mutate or update the original JSON
// therefore, we can actually turn a JS into a JSOn

sampleArr = JSON.stringify(parsedSampleArr);
console.log("sampleArr");
console.log(sampleArr);
// database(JSON) => Server/API (JSON TO JS Object to process) => sent as json (fronted/client)
/*
	Given the Json array, process it and convert to a JS object so we can manipulate the array 

	Delete the last item in the array and add a new item in the array

	stringify the array back in JSON

	update the jsonArr with the stringified array
*/
let jsonArr =`
[
"pizza",
"hamburger",
"shanghai",
"hotdog stick on a pineapple",
"pacit bihon"

]
`;

jsonArray = JSON.parse(jsonArr);
console.log(jsonArray);
console.log(jsonArray.pop());
console.log(jsonArray.push("pancit"));
console.log(jsonArray);
jsonArr = JSON.stringify(jsonArray);
console.log(jsonArr)

// GATHER USER DETAILS 
let firstName = prompt("what is your first name?");
let lastName = prompt("what is your last name");
let age = prompt("what is your age")
let address = {
	city : prompt("From which city do you live in?"),
	country:prompt("from which country does your city address belong to?")
};

let otherData = JSON.stringify({
	firstName:firstName,
	lastName:lastName,
	age:age,
	address:address
})
console.log(otherData);

let sample3 = `
{
	'name':"Cardo",
	"age":18,
	"address":{
		"city":"Quiapo",
		"country":"Philippines"
	}
}

`;

try {
	console.log(JSON.parse(sample3))
}
catch(err){
	console.log("error message")
	console.log(err)
}finally{
	console.log("this will run!")
}