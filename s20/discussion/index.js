// [SECTION]
//     while (expression/condition){
//         statement
//     }

let count = 5;
// while the value of count is not equal to zero
while (count !==0) {
    // the current value of the count prited out 
    console.log("while:" + count);
    count --;
    // decrease the value by 1 after every iteration to stop the loop when it reaches 0
}
// [SECTION] Do While loop 
//  A do-while loop works a lot like a while loop. But unlike while lopps, do-while loops guarantee that the c ode will be executed atleast once
 /*
SYNTAX:
do {
    statement
} while (expression/condition)
*/

// let number = Number(prompt("Give me a number "))
// do {
//     // the current value of number is printed out 
//    console.log("Do while:" + number)
// //    number += 1; //increase the value of number by  1 after every iteration to stop the value loop when it reaches 10  or greater than 10
// } while (number < 10);//providing a number of 10 or greater will run the code block and will stop the loop


// [SECTION ] For Loop
/*
SYNTAX :
    for (initialization;expression/condition;finalExpression){
    statement
    }
*/
for (let count = 0 ;count <=20; count+=1){//++)
   console.log(count); 
};
   /*
// A for loop is more flexible than while and do-while loops. It consists of three parts:
1. The "initialization" value that will track the progression of the loop.
2.  The "expression/condition" that will be evaluated which will determine whether the loop will run one more time.
3. The "finalExpression" indicates how to advance the loop.
*/



//. length property 
let myString = "Saki";
console.log("Result of .length property: " + myString.length);

// accessing element of a string

console.log(myString[0]);
console.log(myString[1]);
console.log(myString[2]);
console.log(myString[3]);

// Loop that will print out the individual letters of the myString variable

for (let i = 0 ;i < myString.length; i+=1){
   console.log(myString[i])
}; 

let myName = "alex"
for (let i = 0; i<myName.length; i+=1 ){
    if(
        myName[i].toLowerCase() == "a"||
        myName[i].toLowerCase() == "e"||
        myName[i].toLowerCase() == "i"||
        myName[i].toLowerCase() == "o"||
        myName[i].toLowerCase() == "u"
        ) {
        console.log(3);
    } else {
        console.log(myName[i])
    };
};

for(let count = 0; count <=20; count+=1 ){ //if remainder = 0
    if (count % 2 === 0 ) {
        //tells the c ode to continue the next iteration of the loop.
        // this ignores all the statements loc ated after the continue statments 

        continue;

    };
    console.log("Continue and breake; "+ count)
    if (count > 10 ){
       // Tells the code to terminate/stope loop even if the expression/condition of the loop defines that should execute so long as the value of count is that or equal to 20
        // number values after 10 will no longer printed
        break;
    }
}


let name ="Alexandro"
for ( let i = 0; i < name.length; i++) {
    if (name[i].toLowerCase()==="a"){
        console.log("continue to the next iteration");
         continue;
    }
    console.log(name[i]);

    if(name[i].toLowerCase()==="r") {
            break; };
}