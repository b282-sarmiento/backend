// console.log("hello world")

// [section]Arithmetic Operators
// Arithmetic Operators allow mathematical operations 

let x = 4 ;
let y = 12;
let sum = x + y;

console.log ("Result of addition operators: " + sum); // result : 16 

let difference = x - y ;
console.log (" Result of subtraction operators:" + difference);

let product = x  * y;
console.log("Result of multiplicaiton operators:" + product);

let quotient = x/y;
console.log ("result of division operators: " + quotient);

let remainder = x % y;
console.log("result of modulo operator:" + remainder);


// [Section ] Assignment operators
// basic assignment operators 
// The assignment operators assigns the value of the right operand  to a variable 
let assignementNumber = 8; 
console.log(assignementNumber)
assignementNumber = assignementNumber + 2 
console.log("result of  addition assignment operators " +assignementNumber)
assignementNumber += 2
console.log("result of  addition assignment operators " + assignementNumber )

// multiple operators and parenthesis
let mdas  = 1 + 2 -3 * 4 / 5
console.log("result of mdas operation " + mdas);

let pemdas =  1 + (2-3) * (4/5);
console.log ("result of pemdas operators " + pemdas)

// increment and decrement
// operators that add or subtact values by 1 and reassigns the value of the variable where the increement and decrement was applied 
 let z = 1 
 let increment = z++;
 console.log ("result of increment: "+ z); // result : 2
 let decrement = z--;
 console.log("result of decrement:" + z)



 // [SECTION] Type Coersion:
// tpe of coercion is the automatic implicit conversion of values from on datat type to another
// Values are automatically converted from one data type to another order to resolve operations
let numA = "10"
let numB = 12;
let coercion = numA + numB;
console.log (coercion); 1012
console.log (typeof coercion); // string

// [SECTION] comparison operators
// Equality Operators (==)
// check whether the operands are eqal/ have the same content
console.log ("equality Operator")
console.log (1 == 1);//true
console.log(1 == "1")// true 
console.log("juan" == 'juan'); // true
console.log(0 == false);// true 

// strick equality operators 
// check whether the operands are equal/ have the same  contents  and also compares the da types of the two values 
console.log ("strick equality Operator")
console.log (1 === 1);//true 
console.log(1 === "1")// false
console.log("juan" === 'juan'); // true
console.log(0 === false);// false
 // inequality operators 
 // checks whether the operands are not eual/have different content
console.log ("inequality Operator")
console.log (1 != 1);//false
console.log(1 != "1")// false 
console.log("juan" != 'juan'); // false
console.log(0 != false);// false

// strick inequality operators 
// checks whether the operands are not equal/ have different contents and also compares the data types of the two values 
console.log ("strick inequality Operator")
console.log (1 !== 1);//false
console.log(1 !== "1")// true 
console.log("juan" !== 'juan'); // false
console.log(0 !== false);// true

// [Section] Relational Operations 
let a = 50 
let b = 65
// Gt or greater than Operator (>)
let isGreaterThan = a > b;// false
// lt or less than operator (<)
let isLessThan = a < b ; // true
// GTE or Greater Than or Equal Operator (>=)
let isGTorGTE = a >= b; // false 
// LTE or Less Than or Equal Operator (<=)
let isLtorLTE = a <= b;// true 
console.log( "Relational Operations")
console.log(isGreaterThan)
console.log(isLessThan)
console.log(isGTorGTE)
console.log(isLtorLTE)

//[SECTION] logical operators 

let isLegalAge = true;
let isRegistered = false;

// logical and Operators (&&)
let allRequirementsMet = isLegalAge && isRegistered
console.log("Result of Logical AND Operator: " + allRequirementsMet)

// Logical or Operator (||)
// Return True if one of the operands is true
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of Logical OR Operator: " + someRequirementsMet)


let someRequirementsNotMet= !isRegistered
console.log("result of Logical NOT(!) Operator: " + someRequirementsNotMet)





