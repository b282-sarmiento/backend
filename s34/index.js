// use the 'require ' directive to laod the express module/package
const express = require("express");
// Create an application using express
// 'app' is our server
const port = 3000;
const app = express();

// method used from express.js are middleware
// -middleware is software that provides service and capabilities to applications outside of whats offered by the operating system.

// allows your app to read json data 
app.use(express.json());
// allows your app to read adata from any forms 
app.use(express.urlencoded({extended:true}));

// [SECTION ] Routes 
// GET

app.get("/greet", (request,response)=>{
	// "response.send " method to send a response back to the client
	response.send("Hello from the /greet endpoint!")
})

// Post
app.post("/hello",(request,response) =>{
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName} !`)
})

// Simple registration 
let users =[]
app.post("/signup",(request,response)=>{
	if(request.body.username !==""&& request.body.password !==""){
		users.push(request.body);

		response.send(`User ${request.body.username} successfully registered!`);
	} else{
		response.send("PLease inpute  Both username and password!")
	}
})
// activity

app.get("/home", (request,response)=>{
	
	response.send("Welcome to the homepage")
})
app.get("/users",(request,response)=>{
	response.send(users);
})


app.delete("/delete-users", (request, response) => {
  const usernameToDelete = request.body.username;
  let userDeleted = false;

  for (let i = 0; i < users.length; i++) {
    if (users[i].username === usernameToDelete) {
      users.splice(i, 1);
      userDeleted = true;
      console.log(`User ${usernameToDelete} has been deleted.`);
      break;
    }
  }

  if (!userDeleted) {
    console.log("User does not exist.");
  } else if (users.length === 0) {
    console.log("No users found.");
  }
});

app.listen(port,() => console.log(`server running at port ${port}`))

