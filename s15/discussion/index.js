// [SECTION] Syntax, Statements, and Comments

// Comments are parts of the code that gets ignored by he language 
// Comments are meant to describe the written code
/*There are two types of comments
	1.Single-line coment- denoted by two slashes
	2.Multi-line - denoted by a slash and asterisk
*/

// Statements
// Statements in programming are instructions that we tell the computer to perfom
// example:
console.log("Hello World");
// whitespace
// whitespace(basically, spaces and line breaks)  	 cam inpact fuctionality in many computer languages but not in js
console. log("Hello World!");

console.
log
(
"hello world!"
 	);

// syntax
// In programming, it is the set of rules that decribe how statements must be constructed.
// example:

// [Section] Variables
// it is use to contain Data
// Any information that is used by an application is stored in what we call a "memory"

// Delcaring variables- tells our device that a variable name is created and is ready to store data.
/*
Syntax:
let/const VarableName;
*/

// console.log(hello)// Cannot access 'hello' before initialization
// let hello; 
// Declaring and initializing variables 
// Initializingg variables - the intacnce when a variable is given it's initial value/starting value
// Syntax L
// let/const varableName= value
// let productName = "desktop computer"




/*
Naming Convention
Camel Case: Words are joined together without spaces, and each word, except the first one, starts with a capital letter. For example, myVariableName.

Pascal Case: Similar to camel case, but the first letter of each word is capitalized. For example, MyVariableName.

Kebab Case: Words are joined together with hyphens (-) and all letters are lowercase. For example, my-variable-name.

Snake Case: Words are written in all lowercase letters and separated by underscores (_). For exam*/



let myVariables="money";
console.log(myVariables)
productName = "desktop Computter"
console.log(productName)

let producePrice=18999;
console.log(producePrice)
const interest = 3.539
console.log(interest);

// Reassigning variable values
// Reassigning variables means changing it's value or previous value into another value
//
/*
Syntax
	variableName = NewValue
*/

productName = "Laptop"
console.log(productName) // result :laptop instead of dekstop computer

// let variable cannt be redecalred within it scope
/*
let friend = "kate"
let friend = "jane" friend' has already been declared
*/
// const variable cannot be updated or re-declared
// const interest = 3.539;
/*interest = 4.4232 Uncaught TypeError: Assignment to constant variable.*/
// Reassigning variables vs. Initializing variables
// Declaration
let supplier ;
// Initialization
supplier = "john Smith Tradings "
console.log(supplier)
// Reassignment
supplier = "Zuit Store"
console.log(supplier); //result: Zuitt Store 
// var - is also declaring variable
// hoisting is Js default behavior of moving initialization to top 

a = 5
console.log(a)
var a;

// a = 5 
// console.log(a)
// let a;

// let/const local global scope
// scope essentially means where these variables are available for use 

// let and cons are blocked scoped
// a block is a chuck of code bounded by {}
let outerVariable = "hello"
{
	let innerVariable = "hello again";
	console.log(innerVariable)
}
console.log(outerVariable);
// console.log(innerVariable); //Uncaught ReferenceError: innerVariable is not defined
// multible variable declaration 
let productCode = "DC017", productBrand = "dell";
/*console.log (productCode, productBrand);
const let = "hello";
console.log(let) Uncaught SyntaxError: let is disallowed as a lexically bound name*/

// [SECTION] DATA TYPES 
// strings are series of characters that create a word, phrase, a sentence or anything relate to creating text . 
// Strings in JS can be written using either single('') or double("") quote
let country = 'Phillipines';
let province = "Metro manila";

// Concatenating strings 
// Multiple string values can be combined to create a single string using the "+" symbol

let fullAdress = province +", "+ country;
console.log(fullAdress);
let greeting = "I live in the " + country;
console.log(greeting)
// Escape charater (\)
//\n refers to creating a new line between text 
let mailAdress = 'Metro Manila \n\n\n Phillipine';
console.log(mailAdress);

let message = "John's employess went home early";
console.log(message); // result: John's employees went home early

message = 'John\'s employess went home early'
console.log(message);

// NUMBER
// Integers / Whole number
let headcount = 26;
console.log(headcount);

// Decimal Numbers/Fractions 
let grade = 98.7;
console.log(grade);

// Exponential Notation 
let planetDistance = 2e10;
console.log(planetDistance);

// Combining number data type and strings
console.log("John's grade last quater is " + grade);
// BOOLEAN values are normally used to store values relating to tthe state of certain things 

let isMarried = false ; 
let inGoodConduct = true; 
console.log("isMarried:" +isMarried);
console.log("inGoodConduct:" + inGoodConduct)
// Arrays 
// Array are special kind of object data type that's used to store multiple values

 /*
SYNTAX: let/const arrayName = [elementA, elementB, elementC,..]

 */
// similar data types
let grades = [98.7, 90, 82, 96.2,99.1,]
console.log(grades);

// different data types 
let details = ["john", "smith",23,true ,grades]
console.log(details)

// object is a special kind of datat type that's used to mimic real world objects/ items 

/*
SYNTAX 
	let/const objectName = {
	propertyA:value,
	propertyB:value,
	}
*/

let person = {
	fullName: "Juan Delacruz",
	age: 35,
	contact:["+639174465","817123 5445"],
	address: {
		houseNumber: '345', city: 'Manila'
	}
}
console.log(person);


console.log(typeof person);
console.log(typeof grades);
console.log(typeof myVariables);
console.log(typeof grade);

// Null
let spouse = null;
console.log(spouse);
// undefined 
let fullName;
console.log(fullName)
