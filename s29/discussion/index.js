// Crud Operations

// Create Operations
// insertion() - Inserts one document to the collections 
db.users.insertOne({
    "firstName": "John",
    "lastName":"smith"
});
// insertMany()insert multiple documents to the collection
db.users.insertMany([
{"firstName":"john","lastName":"Doe"},
{"firstName":"jane","lastName":"Doe"}
]);

// read operation
// find()- get all the inserted users 
db.users.find();

// Retrieving specific Documents
db.users.find ({"lastName":"Doe"});

// Update Operations 
// updateOne()-modify one document
db.users.updateOne(
{
"_id" : ObjectId("648af92ffe1e352b75a8230c")
},
{
    $set:{
        "email":"johnsmith@gmail.com"
    }
}
);

// updateMany() - modify multiple documents  
db.user.updateMany(
  { "lastName": "Doe" },
  { $set: { "isAdmin": false } }
);
// Delete Operation
// deleteMany-deletes multiple documents
db.users.deleteMany({"lastName:Doe"})
db.users.deleteMany({ "lastName": "Doe" });
db.users.deleteOne({ "_id" : ObjectId("648af92ffe1e352b75a8230c")});


// comparison query operators 
// $gt operator 
db.users.find({age:{$gt:50}});
db.users.find({age:{$gte:50}});


// lt
db.users.find({age:{$lt:50}});
db.users.find({age:{$lte:50}});
$ne operator 
/*
Allows us to find documents that have field number values not equall to a specified value
-Synatx:
db.collectionName.find({field:{$ne:Value}})
*/
db.users.find({age:{$ne:82}});

// $in operator 
/*
Allows us to find documents with specific  match criteria one field using diffrent values


SYNTAX :
    db.collectionName.field({field:{$in : value}})
*/
db.users.find({lastName:{$in:["Hawking","Doe"]}})

db.users.find({courses: {$in: ["HTML","React"]}});

// Logical Query Operators 

// $or operator 
/*
Allows us to find documents that match a single criteria from multiple provided criteria

-SYNTAX :
db.collectionName.find({$or:[{fieldA:valueA},{fieldB:valueB}]});
*/


db.users.find({$or: [{firstName:"Neil"},{age:21}]});


//  $and operator 
/*

Allows us to find docuements matching multiple criteria in a single field
-SYNTAX :
db.collectionName.find({$and:[{fieldA:valueB},{fieldB:valueB}]});
*/

db.users.find({$and:[ {age:{$ne:82}},{age:{$ne:76}}]})