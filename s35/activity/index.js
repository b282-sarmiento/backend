const express = require("express");
// "mongoose " is a package that allows creating of  schemas to model our data structures 
// also has access to a number of methods for manipulating our database
const mongoose = require("mongoose");

const app = express();

const port  = 3001;
// Connecting to mongoDb Atlas 
mongoose.connect("mongodb+srv://jlsarmiento1996:saki12345@wdc028-course-booking.9nbzgsx.mongodb.net/s35",
	{
		useNewUrlParser:true,
		useUnifiedTopology:true
	}
);
// Connecting to MongoDb locally
// allows to handle errors when the initial connection is established
let db = mongoose.connection;
// "console.error.bind" allows us to print errors in the browser console and in the terminal
// "connection error" is the message that will display if an error encountered
db.on("error",console.error.bind(console,"connection error"));

// if the connection is successful, "We're connected to the cloud database!"

db.once("open",()=>console.log("We're connected to the cloud database!"))


// Middleware 
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// // [SECTION] Moongoose schema
// // Schemas determine the structure of the documents to be written in the database
// // Schema act as blueprints to our data

// // the "new" create a new Schema
// // Use the Schema() constructore of the Mongoose module to create a new schema object
// const taskSchema = new mongoose.Schema({
// 	name : String,
// 	status : {
// 		type : String,
// 		default : "pending"
// 	}
// })
// // [SECTION]Models
// // Models must be in singular form and first letter is capitalized

// // first parameter of the mongoose model method indicates the collection in where to store the data
// // Second parameter is used to used to specify the schema /blueprint of the documents that will be stored in the MongoDB collection
// const Task = mongoose.model("task",taskSchema);
// /*
// Creating a new task
// 1. Add a functionality to check if there are duplicate tasks
// 	- If the task already exists in the database, we return a message
// 	- If the task doesn't exist in the database, we add it in the database
// 2. The task data will be coming from the request's body
// 3. Create a new Task object with a "name" field/property
// 4. The "status" property does not need to be provided because our schema defaults it to "pending" upon creation of an object
// */

// app.post("/tasks", (req, res) => {
//   Task.findOne({ name: req.body.name }).then((result, err) => {
//     if (result != null && result.name == req.body.name) {
//       return res.send("Duplicate task found!");
//     } else {
//       let newTask = new Task({
//         name: req.body.name
//       });
//       // "save()" method will store the information to the database
//       newTask.save().then((savedTask, saveErr) => {
//         if (saveErr) {
//           return console.error(saveErr);
//         } else {
//           return res.status(201).send("New Task Created!");
//         }
//       });
//     }
//   });
// });


/*
Getting all the tasks
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/

// app.get("/tasks",(req,res)=>{
// 	Task.find({}).then((result,err)=>{
// 		if(err){
// 			return console.log(err);

// 		}else{
// 			return res.status(200).json({
// 				data:result
// 			})
// 		}
// 	})
// })



// [activity s35]
const taskSchema = new mongoose.Schema({
	username : String,
	password : String
})

const Task = mongoose.model("task",taskSchema);
app.post("/signup", (req, res) => {
  if (req.body.username === "") {
    return res.status(400).send("Username and password must be provided.");
  }
 if (req.body.password === ""){
    return res.status(400).send("Username and password must be provided.");
  }
  Task.findOne({ username: req.body.username }).then((result, err) => {
    if (result != null && result.username == req.body.username) {
      return res.send("Duplicate Username found!");
    } else {
      let newTask = new Task({
        username: req.body.username
      });
      newTask.save().then((savedTask, saveErr) => {
        if (saveErr) {
          return console.error(saveErr);
        } else {
          return res.status(201).send("New User Created!");
        }
      });
    }
  });
});

/*logic
Create a user with password and username 
if the username already exist in the database, we return a message
// 	- If the task doesn't exist in the database, we add it in the database

*/





app.listen(port,() => console.log(`server running at port ${port}!`))